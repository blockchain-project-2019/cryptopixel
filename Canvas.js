const pixelCanvas = document.querySelector('.pixel-canvas');
const picker = document.querySelector('.color-picker');
const pixelList = document.querySelector('.pixel_list');


let userAccount;
window.addEventListener('load', function() {
    if (typeof window.web3 !== 'undefined') {
        //console.log('Connecting');
        window.web3 = new Web3(window.web3.currentProvider);
        toast('Connected to MetaMask');
        launch();
    } else {
        alert('You need metamask to continue');
    }
});

const pixelsContractAddress = "0x1BB575aF956cB98C2C56aC95Ec3c6EB9Ae4201A1";
const changedPixels = new Set();
let pixelUpdates = {};
let pixelsContract;

function componentToHex(c) {
    const hex = c.toString(16);
    return hex.length === 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function clearPixels(){
    pixelUpdates = {};
    while (pixelList.firstChild) {
        pixelList.removeChild(pixelList.firstChild);
    }
    resetCanvas();

}

function commit(){
    for(let [pixelId, newColor] of Object.entries(pixelUpdates)){
        pixelsContract.methods.getPixelPrice(pixelId).call().then(price => {
            console.log(window.web3.utils.toWei(price.toString(), 'wei'));
            pixelsContract.methods.paintPixel(pixelId, newColor.r, newColor.g, newColor.b)
                .send({
                    from: userAccount,
                    value: window.web3.utils.toWei(price.toString(), 'wei'),
                    gasLimit: "300000",
                    gasPrice: window.web3.utils.toWei("50", 'gwei')})
                .on("receipt", function(receipt) {
                    toast(`Successfully changed color of a pixel`);
                })
                .on("error", function(error) {
                    toast("Transaction failed");
                    console.log(error);
                });
            pixelUpdates = {};
            while (pixelList.firstChild) {
                pixelList.removeChild(pixelList.firstChild);
            }
            changedPixels.clear();
        });
    }
}

function parseCoords(coord_str){
    const parts = coord_str.split('_');
    return {'x': parseInt(parts[0]), 'y': parseInt(parts[1])}
}

function resetCanvas(){
    pixelsContract.methods.getBoard().call()
        .then(result => {
            console.log(result);
            for (let i = 0; i < result.length; i++) {
                const p = result[i];
                const coords = parseCoords(p._id);
                pixelCanvas.rows[coords.x].cells[coords.y].style.backgroundColor = rgbToHex(parseInt(p.r), parseInt(p.g), parseInt(p.b));
                changedPixels.delete(p._id);
            }
            changedPixels.forEach(id => {
                const coords = parseCoords(id);
                pixelCanvas.rows[coords.x].cells[coords.y].style.backgroundColor = "#ffffff";
            });
            changedPixels.clear();
            toast("Board Loaded");
        });

}

const ethToGwei = eth => eth * 1e18;
const gweiToEth = gwei => gwei / 1e18;

function makeGrid() {
    let gridHeight = 200;
    let gridWidth = 200;

    // Creates rows and cells
    for (let i = 1; i <= gridHeight; i++) {
        let gridRow = document.createElement('tr');

        pixelCanvas.appendChild(gridRow);
        for (let j = 1; j <= gridWidth; j++) {
            let gridCell = document.createElement('td');
            gridRow.appendChild(gridCell);
            gridCell.addEventListener('click', function() {
                const color = picker.value;
                const col = gridCell.cellIndex;
                const row = gridCell.parentElement.rowIndex;
                const ind = `${row}_${col}`;
                if (!changedPixels.has(ind) || pixelUpdates[ind] !== hexToRgb(color) ){
                    changedPixels.add(ind);
                    pixelUpdates[ind] = hexToRgb(color);
                    //console.log(hexToRgb(color));
                    pixelsContract.methods.getPixelPrice(ind).call().then(result =>{
                        //console.log(gweiToEth(result));
                        const liNode = document.createElement("LI");
                        const liText = document.createTextNode(`(${row}, ${col}) | ${color} | ${gweiToEth(result).toFixed(18).replace(/\.?0+$/,"")} ETH`);
                        liNode.appendChild(liText);
                        pixelList.appendChild(liNode);
                    });

                    this.style.backgroundColor = color;
                }
            })
        }
    }
    resetCanvas();
}

function toast(mes) {
    const x = document.getElementById("snackbar");
    x.innerText = mes;
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function launch(){
    window.ethereum.enable().then(accs => {
        userAccount = accs[0];
        console.log(userAccount);
        pixelsContract = new window.web3.eth.Contract(pixelsAbi, pixelsContractAddress);
        /*pixelsContract.events.Log()
            .on("data", function(event) {
                console.log(`Logged: ${event.returnValues.logn}`)
            }).on("error", console.error);*/
        pixelsContract.events.PixelPainted()
            .on("data", function(event) {
                const pix = event.returnValues;
                console.log(pix);
                const coords = parseCoords(pix._id);
                pixelCanvas.rows[coords.x].cells[coords.y].style.backgroundColor = rgbToHex(pix.r, pix.g, pix.b);
            }).on("error", console.error);
        const accountInterval = setInterval(function() {
            window.web3.eth.getAccounts().then(cur_acc => {
                if (cur_acc !== userAccount) {
                    userAccount = cur_acc;
                    //toast(`switched to ${userAccount}`);
                }
            })
        }, 1000*60);
        makeGrid();
    });
}



