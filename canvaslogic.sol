pragma solidity >=0.5.0 <0.6.0;
pragma experimental ABIEncoderV2;
import "./ownable.sol";
import "./safemath.sol";

contract CanvasLogic is Ownable {
    using SafeMath for uint256;
    using SafeMath for uint32;
    using SafeMath for uint16;
    using SafeMath for uint8;
    
    struct Pixel {
        uint8 r;
        uint8 g;
        uint8 b;
        string _id;
        bool isSet;
    }
    event Log(uint256 logn);
    event PixelPainted(uint256 r, uint256 g, uint256 b, string _id);
    uint256 startingPrice = 0.000000001 ether;
    uint256 commission = 0.000000000001 ether;
    uint256 priceMultiplier = 2;
    uint256 totalCommissions = 0 ether;

    string[] public pixels;
    mapping (string => uint256) pixelIdToIndex;
    mapping (string => Pixel) canvas;
    mapping (string => address) pixelToOwner;
    mapping (string => uint256) pixelToLastUpdate;
    mapping (string => uint256) pixelToPrice;
    mapping (address => uint256) ownerPixelCount;
    mapping (address => uint256) balances;


    function createPixel(address _owner, string memory _pixelId) internal{
        uint256 id = pixels.push(_pixelId).sub(1);
        //emit Log(3);
        pixelIdToIndex[_pixelId] = id;
        //emit Log(4);
        pixelToOwner[_pixelId] = _owner;
        //emit Log(5);
    }

    function withdraw(address _from) external {
        require(balances[_from] > 0);
        address payable _fromPayable = address(uint160(_from));
        _fromPayable.transfer(balances[_from].sub(commission));
        balances[_from] = 0 ether;

    }

    function withdrawAllCommission() external onlyOwner {
        address payable _owner = address(uint160(owner()));
        _owner.transfer(totalCommissions);
    }

    function paintPixel(string calldata _pixelId, uint8 r, uint8 g, uint8 b) external payable{
        emit Log(0);
        if(!canvas[_pixelId].isSet) {
            //emit Log(1);
            require(msg.value == startingPrice, "The starting Price is not matching");
            emit Log(1);
            createPixel(msg.sender, _pixelId);
            //emit Log(6);
            ownerPixelCount[msg.sender] = ownerPixelCount[msg.sender].add(1);
            //emit Log(7);

        }
        if (pixelToOwner[_pixelId] != msg.sender){

            require(msg.value == pixelToPrice[_pixelId], "Not enough eth sent");
            //emit Log(3);
            ownerPixelCount[pixelToOwner[_pixelId]] = ownerPixelCount[pixelToOwner[_pixelId]].sub(1);
            //emit Log(10);
            balances[pixelToOwner[_pixelId]] = balances[pixelToOwner[_pixelId]].add(pixelToPrice[_pixelId]).sub(commission);
            //emit Log(11);
            totalCommissions = totalCommissions.add(commission);
            //emit Log(12);
            pixelToOwner[_pixelId] = msg.sender;
            //emit Log(13);
            ownerPixelCount[msg.sender] = ownerPixelCount[msg.sender].add(1);
            //emit Log(14);

        }
        //emit Log(4);
        canvas[_pixelId] = Pixel(r, g, b, _pixelId, true);
        pixelToPrice[_pixelId] = pixelToPrice[_pixelId].mul(priceMultiplier);
        pixelToLastUpdate[_pixelId] = now;
        emit PixelPainted(r, g, b, _pixelId);
    }

    function getPixel(string calldata _pixelId) external view returns (Pixel memory){
        return canvas[_pixelId];
    }

    function getPixelPrice(string calldata _pixelId) external view returns(uint256){
        if (canvas[_pixelId].isSet){
            return pixelToPrice[_pixelId];
        } else {
            return startingPrice;
        }
    }
    
    function getBoard() external view returns(Pixel[] memory){
        Pixel[] memory result = new Pixel[](pixels.length);
        for( uint256 i = 0; i < pixels.length; i++){
            result[i] = canvas[pixels[i]];
        }
        
        return result;
    }
    
    function getBalance(address _id) external view returns(uint256){
        return balances[_id];
    }
    
    function getPixelsByOwner(address _owner) external view returns(string[] memory) {
    string[] memory result = new string[](ownerPixelCount[_owner]);
    uint counter = 0;
    for (uint i = 0; i < pixels.length; i++) {
      if (pixelToOwner[pixels[i]] == _owner) {
        result[counter] = pixels[i];
        counter++;
      }
    }
    return result;
  }
}